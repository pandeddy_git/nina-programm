import csv
import random

def read_groups():
    rows = []
    with open('..\groups.csv', 'r') as file:
        reader = csv.reader(file)
        for row in reader:
            rows += [row]
    return rows

groups = {tmp[0]: [0, tmp[1]] for tmp in read_groups()}



rows = []

with open('table.csv', 'r') as file:
    reader = csv.reader(file)
    for row in reader:
        rows += row

for i, row in zip(range(len(rows)), rows):
    l = [list(groups.keys()).copy(), list(groups.keys()).copy()[2:]]
    random.shuffle(l[0])
    random.shuffle(l[1])
    rows[i] = [row, ' '] + l[0]

with open('..\\table-2.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    for row in rows:
        writer.writerow(row)