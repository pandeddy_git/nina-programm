import csv
import random
from math import inf

OFFSET = ''

GOUT = 2
dict = {}


def read_groups():
    rows = []
    with open(OFFSET + 'groups.csv', 'r') as file:
        reader = csv.reader(file)
        for row in reader:
            rows += [row]
    return rows


def find_best_rec(students, groups, stsel, l, f):
    if students:
        r = {}
        st = students[0]
        if st not in stsel: stsel[st] = []
        max = [[], -inf]
        for g in dict[st]:
            if g not in stsel[st] and groups[g][0] < groups[g][1]:
                if True or g not in l or f < len(students) * 1.5:
                    groups[g] = [groups[g][0] + 1, groups[g][1]]
                    tmp = find_best_rec(students[1:], groups, stsel, l, f + 1)
                    max = [tmp[0] + [st, g], 10 - (dict[st].index(g) ** 2) + tmp[1]]
                    break
                else:
                    gtmp = groups.copy()
                    gtmp[g] = [gtmp[g][0] + 1, gtmp[g][1]]
                    tmp = find_best_rec(students[1:], gtmp, stsel, l, f + 1)
                    r[g] = [tmp[0] + [st, g], 10 - (dict[st].index(g) ** 2) + tmp[1]]
                    if 10 - (dict[st].index(g) ** 2) + tmp[1] > max[1]:
                        max = [tmp[0] + [st, g], 10 - (dict[st].index(g) ** 2) + tmp[1]]

        return max

    else:
        return [[], 0]


def find_best(dict, stsel, l):
    result = []
    students = list(dict.keys())
    random.shuffle(students)
    for i in range(GOUT):
        groups = {tmp[0]: [0, int(tmp[1])] for tmp in read_groups()}
        students.sort(reverse=True, key=(
            lambda x: (sum([dict[x].index(sel) for sel in stsel[x]]) / len(stsel[x])) if x in stsel and stsel[
                x] else 0))
        result.append(find_best_rec(students, groups, stsel, l, 0)[0])
        for i, st in zip(range(len(result[-1][::2])), result[-1][::2]):
            stsel[st] = stsel[st] + [result[-1][i * 2 + 1]]

    l = [0] * 5
    for st, sel in stsel.items():
        for i in [dict[st].index(a) for a in sel]:
            l[i] += 1
        if sum([dict[st].index(a) for a in sel]) > 4: print(st)

    return [l, stsel]


def read():
    rows = []
    l = []
    with open(OFFSET + 'table-2.csv', 'r') as file:
        reader = csv.reader(file)
        for row in reader:
            rows += [row]

    for i, row in zip(range(len(rows)), rows):
        dict[row[0]] = row[2:]
        l += row[2:4]

    groups = {tmp[0]: [0, int(tmp[1])] for tmp in read_groups()}
    l = [g for g, n in groups.items() if n[1] * GOUT < l.count(g)]
    return (rows, l)


def write(dict):
    rows = []
    for key, value in dict.items():
        rows += [[key, ' '] + value]

    with open(OFFSET + 'results.csv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        for row in rows:
            writer.writerow(row)


rows, l = read()
mini = inf
for i in range(500):
    print(i)
    stsel = {}
    tmp = find_best(dict, stsel, l)
    s = sum([tmp[0][i] * (2 ** (2 * i)) for i in range(len(tmp[0]))])
    if s < mini and sum(tmp[0]) == len(dict.keys()) * GOUT:
        mini = s
        #print([tmp[0][i] * i ** 5 for i in range(len(tmp[0]))])
        print(tmp[0])
        write(tmp[1])
